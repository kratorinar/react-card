import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './homepage.css';
import { Button } from '../../components/button/button';

class HomePage extends Component{
    constructor() {
        super()
    
        this.state = {
          posts: [
          ]
        }
    }
    render(){
        return(
            <div className='homepage-container'>
                <h1>Venez trouver le Developpeur qui saura vous aider !</h1>

                <Link to="/archive">
                    <Button />  
                </Link>
                
            </div>
        )
    } 
}

export default HomePage;