import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './index.css';
import HomePage from './page/homepage/homepage';
import Archive from './page/archive/archive';
import { Header } from './components/header/header'

ReactDOM.render(
  <BrowserRouter>
    <Header />
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/archive" element={<Archive />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);
