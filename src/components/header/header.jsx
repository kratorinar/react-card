import React from "react";
import './header.css';
import { Link } from "react-router-dom";

export const Header = () => (
    <header className="header">
        <Link className="route" to="/">Home</Link>
        <Link className="route" to="/Archive">Archive</Link>
    </header>
)