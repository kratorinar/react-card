import React from 'react';

import './card.css';

export function Card({
    company,
    logo, 
    new: nouveau,
    featured,
    position,
    role,
    level,
    postedAt,
    contract,
    location,
    languages,
    tools }) {
    let skill = [role, level, ...languages, ...tools ];
    console.log(skill);

    return  (
    <div className='card-container'>
         <img src={logo} alt="" />
        <section className='card-gauche'>
            <div className='company'>
               <h4> {company} </h4>
            <p> {nouveau} </p>
            <p> {featured} </p> 
            </div>
                <h3> {position} </h3>
            <div className='info'>
                <p>{`${postedAt} - ${contract} - ${location}` } </p>
            </div>
        </section>
        <section className='card-droite'>
            {
               skill.map( (skills) => 
                    <a href="" key={skills.id} > {skills} </a>
                )
            }
        </section>
    </div>
) }  


/* export function Card({position, role}) {

  return (
    <div className="card-container">
      <h3>{position}</h3>
      <p>{role}</p>
    </div>
  )
}
 */